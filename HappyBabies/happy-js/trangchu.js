$(document).ready(function(){
	  $("#owl-demo").owlCarousel({
	     loop:true,
	    margin:0,
	    responsiveClass:true,
	    responsive:{
	        0:{
	            items:1,
	            nav:true
	        },
	    }
	  });
	  $('#owl-demo2').owlCarousel({
      loop:true,
      margin:10,
      nav:true,
      responsive:{
          0:{
              items:1
          	},
          200:{
              items:2
          	},
          769:{
          	  items:3
          	},
          992:{
              items:5
          	}
      	}
  	});
	   $('#owl-demo3').owlCarousel({
    
      margin:10,
      
      responsive:{
          0:{
              items:1
          	},
          200:{
              items:2
          	},
          769:{
          	  items:3
          	},
          992:{
              items:5
          	}
      	}
  	});

	pos =  $(".menu-header").position();
	$(window).scroll(function() {
		var posScroll = $(document).scrollTop();
	    if (parseInt(posScroll) > parseInt(pos.top)) {
	        $(".menu-header").addClass('fix');
	    } else {
	        $(".menu-header").removeClass('fix');
	    }
	});

	$(window).scroll(function() {
	    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
	        $('#return-to-top').fadeIn(200);    // Fade in the arrow
	    } else {
	        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
	    }
	});
	$('#return-to-top').click(function() {      // When arrow is clicked
	    $('body,html').animate({
	        scrollTop : 0                       // Scroll to top of body
	    }, 500);
	});
	$(document).ready(function(){
	  $(".owl-carousel").owlCarousel({
	     loop:true,
	    margin:10,
	    responsiveClass:true,
	    responsive:{
	        0:{
	            items:1,
	            nav:true
	        },
	    }
	  });
	});

  	

});
